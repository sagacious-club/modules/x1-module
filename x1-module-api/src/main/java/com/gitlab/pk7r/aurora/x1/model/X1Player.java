package com.gitlab.pk7r.aurora.x1.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "x1_player")
public class X1Player {

    @Id
    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "wins", nullable = false)
    private int wins = 0;

    @Column(name = "loses", nullable = false)
    private int loses = 0;

    @Column(name = "challenger", nullable = false)
    private boolean challenger = false;

    @Column(name = "challenged", nullable = false)
    private boolean challenged = false;

}
