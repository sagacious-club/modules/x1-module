package com.gitlab.pk7r.aurora.x1.service;

import com.gitlab.pk7r.aurora.x1.model.X1Player;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public interface X1PlayerService {

    Optional<X1Player> getX1Player(String username);

    List<X1Player> getTopX1Players();

    X1Player createX1Player(Player player, boolean challenger, boolean challenged, int wins, int loses);

    void addWin(X1Player x1Player);

    void addLose(X1Player x1Player);

    void setChallenger(X1Player x1Player, boolean challenger);

    void setChallenged(X1Player x1Player, boolean challenged);

    boolean isChallenger(Player player);

    boolean isChallenged(Player player);

}
