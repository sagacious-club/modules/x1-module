package com.gitlab.pk7r.aurora.x1.service;

import com.gitlab.pk7r.aurora.x1.model.X1Arena;
import com.gitlab.pk7r.aurora.x1.model.X1State;
import org.bukkit.entity.Player;

import java.util.Optional;

public interface X1ArenaService {

    Optional<X1Arena> getX1Arena();

    void setCabin(Player player);

    void setPos1(Player player);

    void setPos2(Player player);

    void updateState(X1State newState);

    X1State getState();

}