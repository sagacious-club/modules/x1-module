package com.gitlab.pk7r.aurora.x1.service;

import com.gitlab.pk7r.aurora.x1.model.X1Player;
import org.bukkit.entity.Player;
import org.springframework.data.util.Pair;

public interface ChallengeService {

    Pair<X1Player, X1Player> getChallenge();

    boolean hasChallenge();

    void acceptChallenge(Player player);

    void declineChallenge(Player player);

    void challengePlayer(Player player, Player target);

    void expireChallenge(X1Player challenger, X1Player challenged, boolean cancelled);

    void endChallenge(Player player);

    void winChallenge(Player winner, Player loser);

}
