package com.gitlab.pk7r.aurora.x1.model;

public enum X1State {

    PRE_GAME, IN_GAME, END_GAME, AVAILABLE, CHALLENGE_STARTED

}