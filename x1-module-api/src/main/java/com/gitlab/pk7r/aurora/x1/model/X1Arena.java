package com.gitlab.pk7r.aurora.x1.model;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;

@Getter
@Setter
public class X1Arena {

    private Location cabin;
    private Location pos1;
    private Location pos2;
    private X1State x1State;

}
