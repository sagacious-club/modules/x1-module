package com.gitlab.pk7r.aurora.x1.event;

import com.gitlab.pk7r.aurora.x1.model.X1State;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Getter
@AllArgsConstructor
public class X1UpdateStateEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private X1State state;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
