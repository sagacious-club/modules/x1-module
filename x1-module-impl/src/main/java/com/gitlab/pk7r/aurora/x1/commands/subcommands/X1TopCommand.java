package com.gitlab.pk7r.aurora.x1.commands.subcommands;

import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.x1.service.X1PlayerService;
import dev.jorel.commandapi.CommandAPICommand;

import java.util.concurrent.atomic.AtomicInteger;

public class X1TopCommand {

    public static CommandAPICommand x1TopCommand(X1PlayerService x1PlayerService) {
        return new CommandAPICommand("top")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.x1.command.x1.top")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&6Top 10 X1").formatted());
                    player.spigot().sendMessage(new Message(" ").formatted());
                    AtomicInteger position = new AtomicInteger();
                    position.set(1);
                    x1PlayerService.getTopX1Players().forEach(x1Player ->
                            player.spigot().sendMessage(new Message(String.format("&f%d. &e%s &7» &6%s vitórias"
                                    , position.getAndIncrement()
                                    , x1Player.getUsername()
                                    , x1Player.getWins())).formatted()));
                });
    }
}
