package com.gitlab.pk7r.aurora.x1.commands.subcommands;

import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.x1.service.X1ArenaService;
import dev.jorel.commandapi.CommandAPICommand;
import lombok.val;

public class X1CabinCommand {

    public static CommandAPICommand getCommand(X1ArenaService x1ArenaService) {
        return new CommandAPICommand("camarote")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.x1.command.x1.camarote")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    val arena = x1ArenaService.getX1Arena();
                    if (arena.isEmpty()) {
                        player.spigot().sendMessage(new Message("&cA arena X1 não pode ser acessada no momento.").formatted());
                        return;
                    }
                    player.teleport(arena.get().getCabin());
                    player.spigot().sendMessage(new Message("&aTeletransportado para o camarote da arena X1.").formatted());
                });
    }
}
