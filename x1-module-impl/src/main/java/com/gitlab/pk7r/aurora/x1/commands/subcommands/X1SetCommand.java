package com.gitlab.pk7r.aurora.x1.commands.subcommands;

import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.x1.service.X1ArenaService;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.ArgumentSuggestions;
import dev.jorel.commandapi.arguments.StringArgument;
import lombok.val;

public class X1SetCommand {

    public static CommandAPICommand getInvalidCommand() {
        return new CommandAPICommand("set")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.x1.command.set")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&cUso correto: /x1 set <CAMAROTE/POS1/POS2>").formatted());
                });
    }

    public static CommandAPICommand getCommand(X1ArenaService x1ArenaService) {
        return new CommandAPICommand("set")
                .withArguments(new StringArgument("who").replaceSuggestions(ArgumentSuggestions.strings("CAMAROTE", "POS1", "POS2")))
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.x1.command.set")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    val who = (String) args[0];
                    switch (who.toUpperCase()) {
                        case "CAMAROTE" -> x1ArenaService.setCabin(player);
                        case "POS1" -> x1ArenaService.setPos1(player);
                        case "POS2" -> x1ArenaService.setPos2(player);
                        default -> player.spigot().sendMessage(new Message("&cUso correto: /x1 set <CAMAROTE/POS1/POS2>").formatted());
                    }
                });
    }
}
