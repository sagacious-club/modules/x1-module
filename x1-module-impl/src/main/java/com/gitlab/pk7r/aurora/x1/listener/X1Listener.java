package com.gitlab.pk7r.aurora.x1.listener;

import com.gitlab.pk7r.aurora.Event;
import com.gitlab.pk7r.aurora.configuration.FileConfiguration;
import com.gitlab.pk7r.aurora.service.SchedulerService;
import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.x1.event.X1UpdateStateEvent;
import com.gitlab.pk7r.aurora.x1.model.X1State;
import com.gitlab.pk7r.aurora.x1.service.ChallengeService;
import com.gitlab.pk7r.aurora.x1.service.X1ArenaService;
import com.gitlab.pk7r.aurora.x1.service.X1PlayerService;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Event
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class X1Listener implements Listener {

    Plugin plugin;

    ChallengeService challengeService;

    X1ArenaService x1ArenaService;

    X1PlayerService x1PlayerService;

    SchedulerService schedulerService;

    FileConfiguration fileConfiguration;

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onKillInX1(PlayerDeathEvent event) {
        val dead = event.getEntity();
        val killer = dead.getKiller();
        if (killer != null && (x1PlayerService.isChallenger(dead) || x1PlayerService.isChallenged(dead) ||
                x1PlayerService.isChallenger(killer) || x1PlayerService.isChallenged(killer))) {
            for (ItemStack content : dead.getInventory().getContents()) {
                if (content == null) continue;
                Objects.requireNonNull(killer.getLocation().getWorld()).dropItem(killer.getLocation(), content);
            }
            challengeService.winChallenge(killer, dead);
        }
    }

    @EventHandler
    public void onUpdateX1ArenaState(X1UpdateStateEvent event) {
        if (!event.getState().equals(X1State.AVAILABLE)) {
            val challenger = challengeService.getChallenge().getFirst();
            val challenged = challengeService.getChallenge().getSecond();
            switch (event.getState()) {
                case IN_GAME -> Bukkit.getOnlinePlayers().stream()
                        .filter(player -> player.getName().equalsIgnoreCase(challenger.getUsername()) ||
                                player.getName().equalsIgnoreCase(challenged.getUsername()))
                        .forEach(player -> sendTitle(player, "&a&lLute!", "", 25, 50, 25));
                case PRE_GAME -> new BukkitRunnable() {
                    int timer = 10;

                    @Override
                    public void run() {
                        if (timer == 0) {
                            x1ArenaService.updateState(X1State.IN_GAME);
                            cancel();
                            return;
                        }
                        Bukkit.getOnlinePlayers().stream()
                                .filter(player -> player.getName().equalsIgnoreCase(challenger.getUsername()) ||
                                        player.getName().equalsIgnoreCase(challenged.getUsername()))
                                .forEach(player -> {
                                    if (timer <= 6 && timer > 3) {
                                        sendTitle(player, String.format("&e&l%s", timer), "", 0, 22, 0);
                                    } else if (timer <= 3) {
                                        sendTitle(player, String.format("&c&l%s", timer), "", 0, 22, 0);
                                    } else {
                                        sendTitle(player, String.format("&a&l%s", timer), "", 0, 22, 0);
                                    }
                                });
                        timer--;
                    }
                }.runTaskTimer(plugin, 0L, 20L);
                case CHALLENGE_STARTED ->
                        schedulerService.scheduleSyncDelayedTask(() ->
                                challengeService.expireChallenge(challenger, challenged, false), 60 * 20L * 6);
            }
        }
    }

    @EventHandler
    public void onMoveWhenX1PreGame(PlayerMoveEvent e) {
        val player = e.getPlayer();
        val from = e.getFrom();
        if (from.getZ() != e.getTo().getZ() && from.getX() != e.getTo().getX()) {
            if (x1ArenaService.getState().equals(X1State.PRE_GAME)) {
                if (x1PlayerService.isChallenger(player) || x1PlayerService.isChallenged(player)) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDropWhenX1PreGame(PlayerDropItemEvent event) {
        val player = event.getPlayer();
        if (x1ArenaService.getState().equals(X1State.PRE_GAME)) {
            if (x1PlayerService.isChallenger(player) || x1PlayerService.isChallenged(player)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPickupWhenX1PreGame(EntityPickupItemEvent event) {
        if (event.getEntity() instanceof Player player) {
            if (x1ArenaService.getState().equals(X1State.PRE_GAME)) {
                if (x1PlayerService.isChallenger(player) || x1PlayerService.isChallenged(player)) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onCommandWhenInX1(PlayerCommandPreprocessEvent event) {
        val player = event.getPlayer();
        val command = event.getMessage().split(" ")[0];
        if (x1ArenaService.getState().equals(X1State.IN_GAME) || x1ArenaService.getState().equals(X1State.PRE_GAME)) {
            if (x1PlayerService.isChallenger(player) || x1PlayerService.isChallenged(player)) {
                val allowedCommands = fileConfiguration.getConfiguration("x1.yml").getStringList("allowed-commands");
                if (!allowedCommands.contains(command)) {
                    event.setCancelled(true);
                    event.getPlayer().spigot().sendMessage(new Message("&cVocê não pode usar comandos enquanto está em X1.").formatted());
                }
            }
        }
    }

    @EventHandler
    public void onX1Quit(PlayerQuitEvent event) {
        val player = event.getPlayer();
        if (x1ArenaService.getState().equals(X1State.IN_GAME) || x1ArenaService.getState().equals(X1State.PRE_GAME)) {
            if (x1PlayerService.isChallenger(player) || x1PlayerService.isChallenged(player)) {
                val challenge = challengeService.getChallenge();
                Bukkit.getServer().spigot()
                        .broadcast(new Message(String.format("&7[&c!&7] O jogador &c%s &7deslogou enquanto estava em um X1.",
                                player.getName())).formatted());
                Arrays.stream(Bukkit.getServer().getOfflinePlayers())
                        .filter(offlinePlayer -> offlinePlayer.getName() != null)
                        .filter(offlinePlayer -> List.of(challenge.getFirst().getUsername(),
                                challenge.getSecond().getUsername()).contains(offlinePlayer.getName().toLowerCase()))
                        .filter(offlinePlayer -> !offlinePlayer.getName().equalsIgnoreCase(player.getName()))
                        .forEach(offlinePlayer -> challengeService.winChallenge(offlinePlayer.getPlayer(), player));
            }
        }
    }

    public static void sendTitle(Player player, String title, String subTitle, int a, int b, int c) {
        player.sendTitle(new Message(title).colored().toString(), new Message(subTitle).colored().toString(), a, b, c);
    }
}