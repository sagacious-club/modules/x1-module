package com.gitlab.pk7r.aurora.x1.commands;

import com.gitlab.pk7r.aurora.Command;
import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.x1.commands.subcommands.*;
import com.gitlab.pk7r.aurora.x1.service.ChallengeService;
import com.gitlab.pk7r.aurora.x1.service.X1ArenaService;
import com.gitlab.pk7r.aurora.x1.service.X1PlayerService;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.PlayerArgument;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.entity.Player;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Command
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class X1Command {

    ChallengeService challengeService;

    X1ArenaService x1ArenaService;

    X1PlayerService x1PlayerService;

    @PostConstruct
    public void x1Command() {
        new CommandAPICommand("x1")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.x1.command.x1")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&6X1 - Ajuda\n").formatted());
                    if (player.hasPermission("aurora.x1.command.set")) {
                        player.spigot().sendMessage(new Message("&6- &e/x1 set <CAMAROTE/POS1/POS2>").formatted());
                    }
                    player.spigot().sendMessage(new Message("&6- &e/x1 <jogador>").formatted());
                    player.spigot().sendMessage(new Message("&6- &e/x1 aceitar").formatted());
                    player.spigot().sendMessage(new Message("&6- &e/x1 negar").formatted());
                })
                .withSubcommand(X1AcceptCommand.getCommand(challengeService, x1PlayerService, x1ArenaService))
                .withSubcommand(X1DeclineCommand.getCommand(challengeService, x1PlayerService, x1ArenaService))
                .withSubcommand(X1SetCommand.getCommand(x1ArenaService))
                .withSubcommand(X1SetCommand.getInvalidCommand())
                .withSubcommand(X1StatsCommand.x1StatsCommand(x1PlayerService))
                .withSubcommand(X1StatsCommand.x1StatsFullCommand(x1PlayerService))
                .withSubcommand(X1TopCommand.x1TopCommand(x1PlayerService))
                .withSubcommand(X1CabinCommand.getCommand(x1ArenaService))
                .register();
        new CommandAPICommand("x1")
                .withArguments(new PlayerArgument("jogador"))
                .executesPlayer((player, args) -> {
                    val target = (Player) args[0];
                    if (!player.hasPermission("aurora.x1.command.x1")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (challengeService.hasChallenge()) {
                        player.spigot().sendMessage(new Message("&cJá existe um desafio ativo no momento.").formatted());
                        return;
                    }
                    challengeService.challengePlayer(player, target);
                }).register();
    }
}
