package com.gitlab.pk7r.aurora.x1.service;

import com.gitlab.pk7r.aurora.essentials.service.SpawnService;
import com.gitlab.pk7r.aurora.myth.service.MythService;
import com.gitlab.pk7r.aurora.service.SchedulerService;
import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.x1.model.X1Player;
import com.gitlab.pk7r.aurora.x1.model.X1State;
import com.gitlab.pk7r.aurora.x1.repository.X1PlayerRepository;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

import static com.gitlab.pk7r.aurora.x1.listener.X1Listener.sendTitle;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ChallengeServiceImpl implements ChallengeService {

    X1PlayerRepository x1PlayerRepository;

    MythService mythService;

    X1ArenaService x1ArenaService;

    SpawnService spawnService;

    SchedulerService schedulerService;

    X1PlayerService x1PlayerService;

    @Override
    public Pair<X1Player, X1Player> getChallenge() {
        return Pair.of(x1PlayerRepository.findByChallengerTrue(), x1PlayerRepository.findByChallengedTrue());
    }

    @Override
    public boolean hasChallenge() {
        return x1PlayerRepository.countByChallengerIsTrue() == 1 && x1PlayerRepository.countByChallengedIsTrue() == 1;
    }

    @Override
    public void acceptChallenge(Player player) {
        val challenger = Bukkit.getOnlinePlayers().stream()
                .filter(p -> p.getName().equalsIgnoreCase(getChallenge().getFirst().getUsername())).findFirst();
        val arena = x1ArenaService.getX1Arena();
        if (challenger.isPresent()) {
            if (arena.isEmpty()) {
                player.spigot().sendMessage(new Message("&cA arena X1 não pode ser acessada no momento.").formatted());
                challenger.get().spigot().sendMessage(new Message("&cA arena X1 não pode ser acessada no momento.").formatted());
                return;
            }
            player.teleport(arena.get().getPos2());
            challenger.get().teleport(arena.get().getPos1());
            x1ArenaService.updateState(X1State.PRE_GAME);
            val message = String.format("&eO jogador &6%s&e aceitou o X1 de &6%s&e. &6[[Assistir]](/x1 camarote hover=&6Assistir)"
                    , player.getName(), challenger.get().getName());
            Bukkit.getServer().spigot().broadcast(new Message(message).formatted());
        }
    }

    @Override
    public void declineChallenge(Player player) {
        if (mythService.isMyth(player)) {
            val online = Bukkit.getOnlinePlayers().stream().toList();
            val target = Bukkit.getPlayer(online.get(new Random().nextInt(online.size())).getName());
            mythService.setMyth(target);
        }
        val challenged = x1PlayerService.getX1Player(player.getName()).orElseThrow();
        val challenger = x1PlayerRepository.findByChallengerTrue();
        x1PlayerService.setChallenged(challenged, false);
        x1PlayerService.setChallenger(challenger, false);
        x1ArenaService.updateState(X1State.AVAILABLE);
        val message = String.format("&eO jogador &6%s&e recusou o X1 de &6%s&e.", challenged.getUsername(), challenger.getUsername());
        Bukkit.getServer().spigot().broadcast(new Message(message).formatted());
    }

    @Override
    public void challengePlayer(Player player, Player target) {
        Optional<X1Player> challenger;
        Optional<X1Player> challenged;
        if (x1PlayerService.getX1Player(player.getName()).isEmpty()) {
            x1PlayerService.createX1Player(player, true, false, 0, 0);
        }
        if (x1PlayerService.getX1Player(target.getName()).isEmpty()) {
            x1PlayerService.createX1Player(target, false, true, 0, 0);
        }
        challenger = x1PlayerService.getX1Player(player.getName());
        challenged = x1PlayerService.getX1Player(target.getName());
        x1PlayerService.setChallenger(challenger.orElseThrow(), true);
        x1PlayerService.setChallenged(challenged.orElseThrow(), true);
        x1ArenaService.updateState(X1State.CHALLENGE_STARTED);
        String message;
        if (mythService.isMyth(player)) {
            message = String.format("&eO mito &6%s&e desafiou &6%s&e para um X1.", player.getName(), target.getName());
        } else if (mythService.isMyth(target)) {
            message = String.format("&eO jogador &6%s&e desafiou o mito &6%s&e para um X1.", player.getName(), target.getName());
        } else {
            message = String.format("&eO jogador &6%s&e desafiou &6%s&e para um X1.", player.getName(), target.getName());
        }
        Bukkit.getServer().spigot().broadcast(new Message(message).formatted());
        target.spigot().sendMessage(new Message("[&a[Aceitar]](/x1 aceitar hover=&aAceitar X1) [&c[Negar]](/x1 negar hover=&cNegar X1)").formatted());
    }

    @Override
    public void expireChallenge(X1Player challenger, X1Player challenged, boolean cancelled) {
        if (x1ArenaService.getState().equals(X1State.CHALLENGE_STARTED)) {
            x1ArenaService.updateState(X1State.AVAILABLE);
            x1PlayerService.setChallenged(challenged, false);
            x1PlayerService.setChallenger(challenger, false);
            String message;
            val player = Bukkit.getOnlinePlayers().stream()
                    .filter(p -> p.getName().equalsIgnoreCase(challenger.getUsername())).findFirst();
            val target = Bukkit.getOnlinePlayers().stream()
                    .filter(p -> p.getName().equalsIgnoreCase(challenged.getUsername())).findFirst();
            if (player.isPresent() && mythService.isMyth(player.get())) {
                message = String.format("&eO desafiou entre o mito &6%s&e e &6%s&e %s.",
                        challenger.getUsername(), challenged.getUsername(), cancelled ? "foi cancelado" : "expirou");
            } else if (target.isPresent() && mythService.isMyth(target.get())) {
                message = String.format("&eO desafiou entre &6%s&e e o mito &6%s&e %s.",
                        challenger.getUsername(), challenged.getUsername(), cancelled ? "foi cancelado" : "expirou");
            } else {
                message = String.format("&eO desafiou entre &6%s&e e &6%s&e %s.",
                        challenger.getUsername(), challenged.getUsername(), cancelled ? "foi cancelado" : "expirou");
            }
            Bukkit.getServer().spigot().broadcast(new Message(message).formatted());
        }
    }

    @Override
    public void endChallenge(Player player) {
        val challenger = getChallenge().getFirst();
        val challenged = getChallenge().getSecond();
        x1PlayerService.setChallenged(challenged, false);
        x1PlayerService.setChallenger(challenger, false);
        player.teleport(spawnService.getProperSpawn(player));
    }

    @Override
    public void winChallenge(Player winner, Player loser) {
        if (mythService.isMyth(loser)) {
            val online = Bukkit.getOnlinePlayers().stream().toList();
            val target = Bukkit.getPlayer(online.get(new Random().nextInt(online.size())).getName());
            mythService.setMyth(target);
        }
        val winnerX1Player = x1PlayerService.getX1Player(winner.getName()).orElseThrow();
        val loserX1Player = x1PlayerService.getX1Player(loser.getName()).orElseThrow();
        x1PlayerService.addWin(winnerX1Player);
        x1PlayerService.addLose(loserX1Player);
        sendTitle(winner, "&6&lParabéns!", String.format("&eVocê ganhou o X1 contra &c%s",
                loser.getName()), 25, 50, 25);
        winner.spigot().sendMessage(new Message("&aVocê tem &e10 segundos &apara pegar os itens do chão.").formatted());
        val message = String.format("&eO jogador &6%s&e venceu &6%s&e no X1.", winner.getName(), loser.getName());
        Bukkit.getServer().spigot().broadcast(new Message(message).formatted());
        x1ArenaService.updateState(X1State.END_GAME);
        schedulerService.scheduleSyncDelayedTask(() -> endChallenge(winner), 10 * 20L);
    }
}