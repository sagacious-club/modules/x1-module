package com.gitlab.pk7r.aurora.x1.commands.subcommands;

import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.x1.model.X1State;
import com.gitlab.pk7r.aurora.x1.service.ChallengeService;
import com.gitlab.pk7r.aurora.x1.service.X1ArenaService;
import com.gitlab.pk7r.aurora.x1.service.X1PlayerService;
import dev.jorel.commandapi.CommandAPICommand;

public class X1AcceptCommand {

    public static CommandAPICommand getCommand(ChallengeService challengeService, X1PlayerService x1PlayerService, X1ArenaService x1ArenaService) {
        return new CommandAPICommand("aceitar")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.x1.command.x1.aceitar")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    if (!challengeService.hasChallenge()) {
                        player.spigot().sendMessage(new Message("&cNão existe nenhum desafio ativo no momento.").formatted());
                        return;
                    }
                    if (!x1PlayerService.isChallenged(player)) {
                        player.spigot().sendMessage(new Message("&cVocê não foi desafiado para X1.").formatted());
                        return;
                    }
                    if (x1ArenaService.getState().equals(X1State.IN_GAME) || x1ArenaService.getState().equals(X1State.PRE_GAME)) {
                        if (x1PlayerService.isChallenged(player) || x1PlayerService.isChallenger(player)) {
                            player.spigot().sendMessage(new Message("&cVocê já está em X1.").formatted());
                        } else {
                            player.spigot().sendMessage(new Message("&cA arena X1 está sendo utilizada no momento.").formatted());

                        }
                        return;
                    }
                    challengeService.acceptChallenge(player);
                });
    }

}
