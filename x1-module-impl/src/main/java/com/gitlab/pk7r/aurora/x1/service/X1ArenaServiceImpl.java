package com.gitlab.pk7r.aurora.x1.service;

import com.gitlab.pk7r.aurora.configuration.FileConfiguration;
import com.gitlab.pk7r.aurora.service.LocationSerializationService;
import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.x1.event.X1UpdateStateEvent;
import com.gitlab.pk7r.aurora.x1.model.X1Arena;
import com.gitlab.pk7r.aurora.x1.model.X1State;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class X1ArenaServiceImpl implements X1ArenaService {

    FileConfiguration fileConfiguration;

    LocationSerializationService locationSerializationService;

    @PostConstruct
    public void resetState() {
        updateState(X1State.AVAILABLE);
    }

    @Override
    public Optional<X1Arena> getX1Arena() {
        val cabin = getArenaSection().getString("cabin");
        val pos1 = getArenaSection().getString("pos1");
        val pos2 = getArenaSection().getString("pos2");
        val state = getArenaSection().getString("state");
        if (cabin == null || cabin.equalsIgnoreCase("")) return Optional.empty();
        if (pos1 == null || pos1.equalsIgnoreCase("")) return Optional.empty();
        if (pos2 == null || pos2.equalsIgnoreCase("")) return Optional.empty();
        if (state == null || state.equalsIgnoreCase("")) return Optional.empty();
        val wrappedState = X1State.valueOf(state);
        val arena = new X1Arena();
        arena.setCabin(locationSerializationService.deserializeLocation(cabin));
        arena.setPos1(locationSerializationService.deserializeLocation(pos1));
        arena.setPos2(locationSerializationService.deserializeLocation(pos2));
        arena.setX1State(wrappedState);
        return Optional.of(arena);
    }

    @Override
    public void setCabin(Player player) {
        getArenaSection().set("cabin", locationSerializationService.serializeLocation(player.getLocation().clone()));
        saveAndReloadX1Configuration();
        player.spigot().sendMessage(new Message("&aCamarote da arena X1 redefinido com sucesso.").formatted());
    }

    @Override
    public void setPos1(Player player) {
        getArenaSection().set("pos1", locationSerializationService.serializeLocation(player.getLocation().clone()));
        saveAndReloadX1Configuration();
        player.spigot().sendMessage(new Message("&aPosição 1 da arena X1 redefinida com sucesso.").formatted());
    }

    @Override
    public void setPos2(Player player) {
        getArenaSection().set("pos2", locationSerializationService.serializeLocation(player.getLocation().clone()));
        saveAndReloadX1Configuration();
        player.spigot().sendMessage(new Message("&aPosição 2 da arena X1 redefinida com sucesso.").formatted());
    }

    @Override
    public void updateState(X1State newState) {
        val event = new X1UpdateStateEvent(newState);
        Bukkit.getServer().getPluginManager().callEvent(event);
        getArenaSection().set("state", newState.name());
        saveAndReloadX1Configuration();
    }

    @Override
    public X1State getState() {
        return X1State.valueOf(getArenaSection().getString("state"));
    }

    @NonNull
    private ConfigurationSection getArenaSection() {
        val configuration = fileConfiguration.getConfiguration("x1.yml");
        return Objects.requireNonNullElseGet(configuration.getConfigurationSection("arena"), () -> configuration.createSection("arena"));
    }

    private void saveAndReloadX1Configuration() {
        val configuration = fileConfiguration.getConfiguration("x1.yml");
        configuration.save();
        configuration.reload();
    }
}