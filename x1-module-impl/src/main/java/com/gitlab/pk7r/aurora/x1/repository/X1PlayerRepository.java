package com.gitlab.pk7r.aurora.x1.repository;

import com.gitlab.pk7r.aurora.x1.model.X1Player;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface X1PlayerRepository extends JpaRepository<X1Player, String> {

    @Query("select x from X1Player x where x.challenged = true or x.challenger = true")
    List<X1Player> findCollectionByChallengedIsTrueOrChallengerIsTrue();

    @Query("select (count(x) > 0) from X1Player x where x.challenged = true and x.username = ?1")
    boolean existsByChallengedTrueAndUsername(String username);

    @Query("select (count(x) > 0) from X1Player x where x.challenger = true and x.username = ?1")
    boolean existsByChallengerTrueAndUsername(String username);

    @Query("select x from X1Player x where x.challenger = true")
    X1Player findByChallengerTrue();

    @Query("select x from X1Player x where x.challenged = true")
    X1Player findByChallengedTrue();

    @Query("select count(x) from X1Player x where x.challenger = true")
    long countByChallengerIsTrue();

    @Query("select count(x) from X1Player x where x.challenger = true")
    long countByChallengedIsTrue();

    @Query("select a from X1Player a order by a.wins DESC")
    List<X1Player> findTop10ByWins(Pageable pageable);

}