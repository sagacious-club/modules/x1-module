package com.gitlab.pk7r.aurora.x1.commands.subcommands;

import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.x1.service.X1PlayerService;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.PlayerArgument;
import lombok.val;
import org.bukkit.entity.Player;

public class X1StatsCommand {

    public static CommandAPICommand x1StatsCommand(X1PlayerService x1PlayerService) {
        return new CommandAPICommand("stats")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.x1.command.x1.stats")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    val x1Player = x1PlayerService.getX1Player(player.getName());
                    if (x1Player.isEmpty()) {
                        player.spigot().sendMessage(new Message("&cVocê nunca jogou um X1 para ter estatísticas.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message(String.format("&eEstatísticas X1 &7» &6%s\n", x1Player.get().getUsername())).formatted());
                    player.spigot().sendMessage(new Message(String.format("&f- &eVitórias: &f%s", x1Player.get().getWins())).formatted());
                    player.spigot().sendMessage(new Message(String.format("&f- &eDerrotas: &f%s", x1Player.get().getLoses())).formatted());
                });
    }

    public static CommandAPICommand x1StatsFullCommand(X1PlayerService x1PlayerService) {
        return new CommandAPICommand("stats")
                .withArguments(new PlayerArgument("jogador"))
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.x1.command.x1.stats.other")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    val target = (Player) args[0];
                    val x1Player = x1PlayerService.getX1Player(target.getName());
                    if (x1Player.isEmpty()) {
                        player.spigot().sendMessage(new Message("&cEste jogador nunca jogou um X1 para ter estatísticas.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message(String.format("&eEstatísticas X1 &7» &6%s\n", x1Player.get().getUsername())).formatted());
                    player.spigot().sendMessage(new Message(String.format("&f- &eVitórias: &f%s", x1Player.get().getWins())).formatted());
                    player.spigot().sendMessage(new Message(String.format("&f- &eDerrotas: &f%s", x1Player.get().getLoses())).formatted());
                });
    }
}
