package com.gitlab.pk7r.aurora.x1.service;

import com.gitlab.pk7r.aurora.x1.model.X1Player;
import com.gitlab.pk7r.aurora.x1.repository.X1PlayerRepository;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.entity.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class X1PlayerServiceImpl implements X1PlayerService {

    X1PlayerRepository x1PlayerRepository;

    @EventListener(ContextRefreshedEvent.class)
    public void resetChallenges() {
        x1PlayerRepository.findCollectionByChallengedIsTrueOrChallengerIsTrue().forEach(c -> {
            setChallenged(c, false);
            setChallenger(c, false);
        });
    }

    @Override
    public Optional<X1Player> getX1Player(String username) {
        return x1PlayerRepository.findById(username.toLowerCase());
    }

    @Override
    public List<X1Player> getTopX1Players() {
        return x1PlayerRepository.findTop10ByWins(PageRequest.of(0, 10));
    }

    @Override
    public X1Player createX1Player(Player player, boolean challenger, boolean challenged, int wins, int loses) {
        val x1Player = new X1Player();
        x1Player.setUsername(player.getName().toLowerCase());
        x1Player.setChallenger(challenger);
        x1Player.setChallenged(challenged);
        x1Player.setWins(wins);
        x1Player.setLoses(loses);
        return x1PlayerRepository.save(x1Player);
    }

    @Override
    public void addWin(X1Player x1Player) {
        x1Player.setWins(x1Player.getWins() + 1);
        x1PlayerRepository.save(x1Player);
    }

    @Override
    public void addLose(X1Player x1Player) {
        x1Player.setLoses(x1Player.getLoses() + 1);
        x1PlayerRepository.save(x1Player);
    }

    @Override
    public void setChallenger(X1Player x1Player, boolean challenger) {
        x1Player.setChallenger(challenger);
        x1PlayerRepository.save(x1Player);
    }

    @Override
    public void setChallenged(X1Player x1Player, boolean challenged) {
        x1Player.setChallenged(challenged);
        x1PlayerRepository.save(x1Player);
    }

    @Override
    public boolean isChallenger(Player player) {
        return x1PlayerRepository.existsByChallengerTrueAndUsername(player.getName().toLowerCase());
    }

    @Override
    public boolean isChallenged(Player player) {
        return x1PlayerRepository.existsByChallengedTrueAndUsername(player.getName().toLowerCase());
    }
}