create table if not exists x1_player (
    username   varchar(255) not null,
    wins       integer      not null,
    loses      integer      not null,
    challenger boolean      not null,
    challenged boolean      not null,
    constraint pk_x1_player primary key (username)
);